package com.international.international.international.controller;

import com.international.international.international.i18n.Resources;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 */
@RestController
@RequestMapping("/international")
public class DemoController {
    @GetMapping("/demo")
    public String demo(){
        String hello = Resources.getMessage("hello");
        System.out.println(hello);
        return hello;
    }

}
